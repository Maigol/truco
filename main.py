from classes import *
from random import randint

# Organize card structure
clubs = ["Basto", "Oro", "Copa", "Espada"]
cards = {"Basto": [], "Oro":    [],
         "Copa":  [], "Espada": []}

# Create game cards, without 8's or 9's
#  and add them to the dictionary
for club in clubs:
    for n in range(1, 13):
        if n not in [8, 9]:
            card = Card(n, club, 0)
            cards[club].append(card)

# Assign the right arbitrary value to each one
for club in clubs:
    cards[club][1].value  = 9
    cards[club][2].value  = 10
    cards[club][3].value  = 1
    cards[club][4].value  = 2
    cards[club][5].value  = 3
    cards[club][7].value  = 5
    cards[club][8].value  = 6
    cards[club][9].value  = 7

cards["Espada"][0].value  = 14
cards["Basto"] [0].value  = 13
cards["Oro"]   [0].value  = 8
cards["Copa"]  [0].value  = 8
cards["Oro"]   [6].value  = 11
cards["Espada"][6].value  = 12
cards["Basto"] [6].value  = 4
cards["Copa"]  [6].value  = 4


# Create deck
deck = Deck()
for club in clubs:
    for card in cards[club]:
        deck.add(card)


# Create players
player  = Player("Fierro")
player2 = Player("Bizcacha")
players = [player, player2]

# Game setup
deck.shuffle()
# Give 3 cards to each player
deck.give(players)

scoreMax = 15 # or 30 if you want
scoreboard = {player:  0,
              player2: 0}

# TODO: Implement Envido
#                 Truco
#                 Flor

# Game action
while True:
    player.print_cards()

    print("    ¿Qué haces?")
    choice = int(input("    Tirar carta Nº ")) - 1

    if not choice in range(len(player.cards)):
        print("/!\\ Solo tenés", len(player.cards), "cartas. Elegí una. /!\\\n")
        continue

    pCards = {} # Played cards
    pCards[player] = player.use_card(choice)
    print("* Tirás el", pCards[player].print_card())

    # Player 2 picks a random card
    pCards[player2] = player2.use_card( randint(0, len(player2.cards)-1) )
    print("*", player2.name, "tira el", pCards[player2].print_card())

    # If the two players use cards of the same value,
    #   whoever is hand (Player.isHand) will win.
    if pCards[player].value == pCards[player2].value:
        if player.isHand:
            winner = player
        else:
            winner = player2
    else:
        # Get the greater card;
        # Sort pCards by the value of the Card objects
        winner = max(pCards, key=lambda x: pCards[x].value)

    winner.score += 1
    print("Punto para", winner.name, '\n')

    # Check round score
    #  If a player's score gets to 2, they win the round
    if winner.score == 2:
        scoreboard[winner] += 1
        print(winner.name, "gana la mano")
    # Check game score
    # TODO: Replace with setter and check every time
    #        the scoreboard is changed. (Allowing premature game-end)
    if max(scoreboard.values()) >= scoreMax:
        # True winner (first to get to 15/30 points in `scoreboard`)
        winner = max(scoreboard, key=lambda x: scoreboard[x])
        print(winner.name.upper(), "GANA LA PARTIDA!")
    
    # TODO: FINISH implementing multiple rounds
