class Card:
    """
    Cards are the key element
    of the game. They have:
    - A display number (num)
    - A club
    - A real, arbitrary value, used when comparing
        with other cards.
    """
    def __init__(self, num, club, value):
        self.num = num
        self.club = club
        self.value = value

    def print_card(self):
        name = ""
        if self.num == 1:
            name += "Ancho de "
        elif self.num == 10:
            name += "Sota de "
        elif self.num == 11:
            name += "Caballo de "
        elif self.num == 12:
            name += "Rey de "
        else:
            name += str(self.num) + " de "

        name += self.club

        return name

    def print_stats(self):
        print("[" + str(self.club) + ":", self.num, "(" + str(self.value) + ")]")

    # TODO: Replace with magic method (?
    def max(card1, card2):
        values = {card1: card1.value,
                  card2: card2.value}
        return max(values)

class Player:
    """
    Pretty self-explainatory. Has 
    - Display name: `name`
    - List of available cards to play: `cards`
    - In case of a draw when comparing cards,
        `isHand` determines the winner (whoever is hand)
    - Per-round score: `score`
    """
    def __init__(self, name, cards = None, isHand = None):
        self.name = name
        self.cards = cards or []
        self.isHand = isHand or False
        self.score = 0

    def take_cards(self, cards):
        if type(cards) == list:
            self.cards.extend(cards)
        elif type(cards) == Card:
            self.cards.append(cards)

    def use_card(self, pos):
        return self.cards.pop(pos)

    def print_cards(self):
        print(self.name, "tiene:")
        for c in self.cards:
            c.print_stats()
        print()


class Deck:
    """
    Deck defines methods to manipulate
    a list of cards
    """
    def __init__(self, cards = None):
        self.cards = cards or []

    def shuffle(self):
        from random import shuffle
        shuffle(self.cards)

    def give(self, player, amount = 3):
        for i in range(amount):
            if type(player) == list:
                for p in player:
                    # List.pop() removes the element returned (last item in list)
                    p.take_cards(self.cards.pop())
            elif type(player) == Player:
                player.take_cards(self.cards.pop())

    def add(self, cards):
        if type(cards) == list:
            self.cards.extend(cards)
        elif type(cards) == Card:
            self.cards.append(cards)


# WIP... apparently
class Board:
    """
    The board performs game logic (like comparing cards)
    and keeps track of the scoreboard.

    `score`: A dict where each key contains a player's name
              and each value is the player's score.
    """
    def __init__(self, players):
        self.players = players
        self.score = {}
        for player in players:
            self.score[player.name] = 0
        #self.cards

    #def compare
